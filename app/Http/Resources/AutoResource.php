<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AutoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'mark_id' => $this->mark_id,
            'model_id' => $this->model_id,
            'year_made' => $this->year_made,
            'run' => $this->run,
            'color' => $this->color,
        ];
    }
}
