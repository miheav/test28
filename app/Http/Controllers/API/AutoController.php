<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\AutoResource;
use Illuminate\Http\Request;
use App\Models\Auto;
use Validator;

class AutoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $collection = Auto::get();

        return $this->sendResponse(AutoResource::collection($collection), 'Post retrieved successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($input, [
            'mark_id' => 'required',
            'model_id' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }



        $post = Auto::create($input);



        return $this->sendResponse(new AutoResource($post), 'Post created successfully.');
    }

    public function update(Request $request, Auto $auto)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'mark_id' => 'required',
            'model_id' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $auto->mark_id = $input['mark_id'];
        $auto->model_id = $input['model_id'];
        $auto->year_made = $input['year_made'];
        $auto->run = $input['run'];
        $auto->color = $input['color'];
        $auto->save();

        return $this->sendResponse(new AutoResource($auto), 'Post updated successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $auto = Auto::where('id', $id)->first();

        if (is_null($auto)) {
            return $this->sendError('Auto not found.');
        }

        return $this->sendResponse(new AutoResource($auto), 'Post retrieved successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Auto $auto)
    {
        $auto->delete();

        return $this->sendResponse([], 'Post deleted successfully.');
    }
}
