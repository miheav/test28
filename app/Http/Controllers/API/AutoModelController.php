<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\MarkResource;
use App\Models\AutoModel;
use Illuminate\Http\Request;

class AutoModelController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $collection = AutoModel::get();


        return $this->sendResponse(MarkResource::collection($collection), 'Post retrieved successfully.');
    }
}
