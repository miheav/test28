<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\AutoMark;
use Illuminate\Http\Request;
use App\Http\Resources\MarkResource;

class AutoMarkController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $collection = AutoMark::get();


        return $this->sendResponse(MarkResource::collection($collection), 'Post retrieved successfully.');
    }

}
