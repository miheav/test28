<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auto extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'mark_id',
        'model_id',
        'year_made',
        'run',
        'color'
    ];
}
