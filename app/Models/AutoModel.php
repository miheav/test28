<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoModel extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'mark_id',
    ];
}
