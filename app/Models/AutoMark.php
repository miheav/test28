<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoMark extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
}
